<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('username')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('remember_token', 80)
                ->unique()
                ->nullable()
                ->default(null);
            $table->string('api_token', 80)
                ->unique()
                ->nullable()
                ->default(null);
              //Etat_civil
              $table->string('nom',50)->nullable();
              $table->string('prenom',50)->nullable();
              $table->date('date_naissance')->nullable();
              $table->string('promotion',4)->nullable();
              $table->text('description')->nullable();
              $table->string('avatar')->nullable();
              $table->enum('statut_marital',["Célibataire","Marié(e)","Veuf(ve)","Concubinage"])->default("célibataire");
              $table->enum('civilite',['Dr','M.','Mlle','Mme','Pr'])->default('M.');
              $table->string('confirm_token', 80)
                  ->unique()
                  ->nullable()
                  ->default(null);
              $table->boolean('confirmed')->default(false);
              $table->dateTime('last_sign_out_at')->nullable();
              $table->dateTime('last_sign_in_at')->nullable();
              $table->string('cv')->nullable();
              //contact
              $table->string('phone1')->nullable();
              $table->string('phone2')->nullable();
              $table->string('siteweb')->nullable();
              $table->string('facebook')->nullable();
              $table->string('linkedin')->nullable();
              $table->string('twitter')->nullable();

              //activite pro

              $table->unsignedBigInteger('pays_residence_id')->nullable();
              $table->unsignedBigInteger('secteur_activite_id')->nullable();
              $table->unsignedBigInteger('formation_id')->nullable();
              $table->string('derniere_profession')->nullable();
              $table->string('activite_actuelle')->nullable();
              $table->unsignedBigInteger('created_by')->nullable();

              $table->enum('type',["admin","membre"])->default('membre');

              $table->foreign('secteur_activite_id')->references('id')->on('secteur_activites')->onDelete('set null');
              $table->foreign('formation_id')->references('id')->on('formations')->onDelete('set null');
              $table->foreign('pays_residence_id')->references('id')->on('pays')->onDelete('set null');
              $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');

              $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Schema::dropIfExists('users');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
