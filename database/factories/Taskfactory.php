<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use App\User;
use App\Projet;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    $projet = Projet::inRandomOrder()->first();
    $responsable = User::inRandomOrder()->first();
    $creatable = $faker->randomElement([
        App\User::class, App\User::class
    ]);

    $done = $faker->boolean;

    return [
        'name' => $faker->word,
        'responsable_id' => $responsable ? $responsable->id : factory(App\User::class),
        'projet_id' => $projet ? $projet->id : factory(App\Projet::class),
        'description' => $faker->text,
        'date_debut_prev' => $faker->date,
        'date_fin_prev' => $faker->date,
        'avancement' => $done ? 100 : $faker->numberBetween($min = 0, $max = 99),
        'statut' => $faker->randomElement(["En attente","En cours", "Achevée", "Abandonnée"]),
        'done' => $done,
        'creatable_id' => $creatable == "App\User" ? factory(App\User::class) : factory(App\User::class),
        'creatable_type' => $creatable
    ];
});
