<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Formation;
use App\User;
use App\SecteurActivite;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Relations\Relation;

$factory->define(App\Entreprise::class, function (Faker $faker) {

    $pays = Formation::inRandomOrder()->first();
    $secteur = SecteurActivite::inRandomOrder()->first();
    $responsable = User::inRandomOrder()->first();
    $creatable = $faker->randomElement([
        App\User::class
    ]);

    return [
        'nom_commercial' => $faker->company,
        'logo' => null,
        'forme_juridique' => $faker->randomElement(["EI","SUARL/EURL","SARL","SAS","SASU","SA","SNC","SCS","GIE","Autre","SP","SCI"]),
        'secteur_activite_id' => $secteur ? $secteur->id : factory(App\SecteurActivite::class),
        'responsable_id' => $responsable ? $responsable->id : factory(App\User::class),
        'pays_id' => $pays ? $pays->id : factory(App\Pays::class),
        'secteur_activite_principal' => $faker->word,
        'autre_activite' => $faker->text,
        'effectif' => $faker->randomElement(['1-5', '6-10', '11-15', '16-20', '+40']),

        //Erreur
        'phone1' => $faker->phoneNumber,
        'phone2' => $faker->phoneNumber,
        'email' => $faker->safeEmail,
        'siteweb' => $faker->domainName  ,
        'facebook' => "https://www.facebook.com/".$faker->word,
        'twitter' => "https://www.twitter.com/".$faker->word,
        'linkedin' => "https://www.linkedin.com/".$faker->word,

        'rue' => $faker->streetName ,
        'nr' => $faker->buildingNumber,
        'bp' => $faker->postcode ,
        'quartier' => $faker->word,
        'ville' => $faker->city,
        'creatable_id' => 1,
        'creatable_type' => array_search($creatable, Relation::$morphMap),
    ];
});
