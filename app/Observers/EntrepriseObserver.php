<?php

namespace App\Observers;

use App\FluxFinance;
use App\Entreprise;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Log;
use Illuminate\Support\Str;

class EntrepriseObserver
{
    /**
     * Handle the Entreprise "created" event.
     *
     * @param  \App\Entreprise  $Entreprise
     * @return void
     */
    public function created(Entreprise $Entreprise)
    {

    }

    /**
     * Handle the Entreprise "updated" event.
     *
     * @param  \App\Entreprise  $Entreprise
     * @return void
     */
    public function updated(Entreprise $Entreprise)
    {
    }

    /**
     * Handle the Entreprise "deleted" event.
     *
     * @param  \App\Entreprise  $Entreprise
     * @return void
     */
    public function deleted(Entreprise $Entreprise)
    {

    }

    /**
     * Handle the Entreprise "restored" event.
     *
     * @param  \App\Entreprise  $Entreprise
     * @return void
     */
    public function restored(Entreprise $Entreprise)
    {
        //
    }

    /**
     * Handle the Entreprise "force deleted" event.
     *
     * @param  \App\Entreprise  $Entreprise
     * @return void
     */
    public function forceDeleted(Entreprise $Entreprise)
    {
        //
    }

    
}
