<?php

namespace App\Http\Requests;

use App\Rules\LocationDisponible;
use App\StatutPayement;
use Illuminate\Foundation\Http\FormRequest;
use Log;

class ProjetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titre' =>"required",
            // 'avancement' =>"required|numeric|min:0|max:100",
            'budget' =>"numeric|min:0",
            'date_debut_prev' =>"required|date|before_or_equal:date_fin_prev",
            'date_fin_prev' =>"required|date|after_or_equal:date_debut_prev",
            'description' =>"required",
            'responsable_id' =>"required",
            // 'entreprise_id' =>"required",
        ];
    }


    public function messages()
    {
        return[
            "date_debut_prev.required"=>"La date de début est requise",
            "date_debut_prev.date"=>"Le format de la date est incorrect",
            "date_debut_prev.before_or_equal"=>"La date de début ne doit pas dépasser celle de fin",
            "date_fin_prev.required"=>"La date de fin est requise",
            "date_fin_prev.date"=>"Le format de la date est incorrect",
            "date_fin_prev.after_or_equal"=>"La date de fin doit dépasser celle de début",
            'titre.required' =>"Le nom du projet est requis",
            'budget.numeric' =>"Le budget doit être un nombre",
            'budget.min' =>"Le budget doit être un nombre positif",
            'avancement.min' =>"L'avancement doit être supérieur à 0",
            'avancement.max' =>"L'avancement doit être inferieur à 100",
            'budget.require' =>"Le budget est requis",
            'description.require' =>"Le budget est requis",

        ];
    }
}
