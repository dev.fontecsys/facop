<?php

namespace App\Http\Controllers;

use App\User;
use App\Projet;
use App\Entreprise;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd($this->statStatutProjet());
        $projet = $this->countProjet();
        return [
            "nombre_projet"=>$projet,
            "projet_en_cours"=>$this->countProjetEncours('ouvert'),
            "projet_termine"=>$this->countProjetEncours('fermé'),
            "nombre_entreprise"=>$this->countEntreprise(),
            'secteur_plus_represente'=>$this->SecteurLePlusRepresente(),
            "effectif_moyen" => 5,
            "effectif_total" => 6,
            "nombre_membre"=>$this->countMembre(),
            "secteur"=>$this->statEntreprise(),
            'statPromotion'=>$this->statPromotion(),
            'statStatutProjet'=>$this->statStatutProjet()

        ];
    }

    private function countProjet(){
        return Projet::all()->count();
    }

    private function countProjetEncours($status){
        return Projet::whereStatut($status)->count();
    }

    private function countEntreprise(){
        return Entreprise::all()->count();
    }

    private function SecteurLePlusRepresente(){
        return Entreprise::all()->groupBy('effectif')->max();
    }

    private function countMembre(){
        return User::all()->count();
    }

    private function statEntreprise(){
        return DB::table('entreprises')
        ->select(DB::raw('count(*) as NbEntreprise, secteur_activites.libelle'))
        ->join('secteur_activites', 'entreprises.secteur_activite_id', '=', 'secteur_activites.id')
        ->groupBy('secteur_activites.libelle')->get();
    }

    private function statPromotion(){
        return DB::table('users')
        ->select(DB::raw('count(*) as Npromotion, users.promotion'))
        ->groupBy('users.promotion')->get();
        // return User::all()->countBy('promotion');
    }
    private function statStatutProjet(){
        $annee = now('y');
        return DB::table('projets')
        ->whereYear('created_at',$annee)
        ->select (DB::raw('count(id) as `data`'), DB::raw('YEAR (created_at) year, MONTH (created_at) month'))
        ->groupby('year' , 'month')->get();
    }

}
