<?php

namespace App\Http\Controllers\Auth;
use App\User;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8','confirmed'],
            'prenom' => ['required', 'string', 'max:255'],
            'civilite' => ['required', 'string'],
            'pays' => ['required'],
            'secteur' => ['required'],
            'promotion' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $membre = User::create([
            'nom' => $data['nom'],
            'name' => $data['nom'],
            'prenom' => $data['prenom'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'civilite' => $data['civilite'],
            'pays_residence_id' => $data['pays'],
            'secteur_activite_id' => $data['secteur'],
            'promotion' => $data['promotion'],
            // 'confirm_token' => Str::random(80),
        ]);
        $membre->nationalites()->attach($data['pays']);
        return $membre;

    }

        // /**
        //  * Handle a registration request for the application.
        //  *
        //  * @param  \Illuminate\Http\Request  $request
        //  * @return \Illuminate\Http\Response
        //  */
        // public function register(Request $request)
        // {
        //     $this->validator($request->all())->validate();

        //     event(new Registered($user = $this->create($request->all())));

        //     return response()->json("valider");
        // }

        // public function showAdminRegisterForm()
        // {
        //     return view('auth.register', ['url' => 'admin']);
        // }

        // protected function createAdmin(Request $request)
        // {
        //     $this->validator($request->all())->validate();
        //     $admin = User::create([
        //         'name' => $request['name'],
        //         'email' => $request['email'],
        //         'password' => Hash::make($request['password']),
        //     ]);
        //     return redirect()->intended('login/admin');
        // }


}
