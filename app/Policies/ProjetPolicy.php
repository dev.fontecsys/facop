<?php

namespace App\Policies;

use App\User;
use App\Projet;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjetPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any projets.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
      return true;
    }

    /**
     * Determine whether the user can view the projet.
     *
     * @param  \App\User  $user
     * @param  \App\Projet  $projet
     * @return mixed
     */
    public function view(User $user, Projet $projet)
    {
        return true;
    }

    /**
     * Determine whether the user can create projets.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the projet.
     *
     * @param  \App\User  $user
     * @param  \App\Projet  $projet
     * @return mixed
     */
    public function update(User $user, Projet $projet)
    {
        return $user->id === $projet->creatable_id || $user->type === "admin" || $user->id === $projet->responsable_id
        ? Response::allow()
        : Response::deny('Vous n\'etes pas autorisez a effectuer cette action');
    }

    /**
     * Determine whether the user can delete the projet.
     *
     * @param  \App\User  $user
     * @param  \App\Projet  $projet
     * @return mixed
     */
    public function delete(User $user, Projet $projet)
    {
        return $user->id === $projet->creatable_id || $user->type === "admin" || $user->id === $projet->responsable_id
        ? Response::allow()
        : Response::deny('Vous n\'etes pas autorisez a effectuer cette action');
    }

    /**
     * Determine whether the user can restore the projet.
     *
     * @param  \App\User  $user
     * @param  \App\Projet  $projet
     * @return mixed
     */
    public function restore(User $user, Projet $projet)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the projet.
     *
     * @param  \App\User  $user
     * @param  \App\Projet  $projet
     * @return mixed
     */
    public function forceDelete(User $user, Projet $projet)
    {
        //
    }
}
