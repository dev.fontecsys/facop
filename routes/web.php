<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
// Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');

// Route::post('/login/admin/', 'Auth\LoginController@adminLogin');
// Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
Auth::routes();
Auth::routes(['verify'=>true]);
// Auth::routes(['register' => true,'verify' => true]);


Route::get('utilisateurs/export/', 'UserController@export');
Route::middleware('auth')->get('historiques/export', 'HistoriqueController@export');

route::post('/register', 'Auth\RegisterController@register');
Route::middleware('auth')->get('/dispatcher/options/{page}', 'OptionDispatcherController@get');
Route::get('auth/options/{page}', 'AuthDependenceController@get');




Route::middleware('auth')->get('profil', 'ProfilController@index');
Route::middleware('auth')->get('profil/activites', 'ProfilController@activities');

Route::middleware('auth')->patch('profil/password', 'ProfilController@password');
Route::middleware('auth')->patch('profil/info', 'ProfilController@info');
Route::middleware('auth')->patch('profil/avatar', 'ProfilController@avatar');

Route::middleware('auth')->get('historiques', 'HistoriqueController@index');
Route::middleware('auth')->get('recherche', 'SearchController@search');
Route::middleware('auth')->get('/membres/memberAsParameter', 'MembreController@getAsParams');
Route::middleware('auth')->get('/entreprises/enterpriseAsParameter', 'EntrepriseController@getAsParams');

Route::middleware('auth')->get('/projets/projetAsParameter', 'ProjetController@getAsParams');




Route::group(['middleware' => ['auth']], function()
{
    Route::prefix('api')->group(function () {
        Route::resources(
            [
                'utilisateurs'=>"UserController",
                'roles'=>"RoleController",
                'membres'=>"MembreController",
                //controller utilisé comme controller d'envoi des msg
                'depenses'=>"DepenseController",
                'entreprises'=>"EntrepriseController",
                'pays'=>"PaysController",
                'options'=>"OptionController",
                'projets'=>"ProjetController",
                'tasks'=>"TasksController",
                'dashboard' => "DashboardController@index",
                'comments'  =>  "CommentController",
                'conversations'=> "ConversationController"
            ]
        );
    });
});

// Route::get('/login','Auth\LoginController@showLoginForm ')->name('login');
Route::get('/{any}', 'HomeController@index')->where('any', '.*');


Route::middleware('auth')->get('/api/dashboard','DashboardController@index');
Route::middleware('auth')->get('/dashboard','DashboardController@index');

